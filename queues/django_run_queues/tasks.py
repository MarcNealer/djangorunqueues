from models import DjangoRunQueue
def add(taskid,taskname,queuename,taskArgs):
    try:
        if not DjangoRunQueue.objects.filter(TaskId=taskid).exists():
            rec=DjangoRunQueue.objects.create(TaskName=taskname,TaskQueue=queuename,TaskArgs=taskArgs)
            return 'Task %d has been added' % rec.id
        else:
            return "Already queued"
    except Exception as e:
        return e

def get_result(taskid):
    try:
        rec=DjangoRunQueue.objects.select_for_update(nowait=True).filter(id=taskid)
        if rec.Proccessed:
            return rec.TaskResults
        else:
            return "Not proccessed Yet"
    except:
        return "Still being proccessed"