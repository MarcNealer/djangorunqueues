=================
DJANGO RUN QUEUES
=================

DRQ is a very simple app. It allows you to add Tasks for a given queue with parameters. You then run a command,
via cron to process a given queue at specific times. The command will proccess up to 10 tasks before stopping

Thus you can add tasks to different queues, and proccess the queues when and how often you want
Quick start
-----------

1. Add "django_run_queues" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'django_run_queues',
    ]

2. Create a file to hold all the tasks that you intend to run with DRQ

3. Add setting DRQ_TASKS_LIB pointing towards this file

4. Run python manage.py migrate to create the model needed

5. Add tasks to the queue using tasks.add()


Adding Tasks to the Task Lib

DRQ expects to find all tasks to be run in a single file. You define that file with setting DRQ_TASKS_LIB.
DRQ will record results of the task run, so remember to return results


Add a Task to Run in a Queue

There is no set-up for queues. This is just a group marker used by the RunQueue command.
Add a task to be run using the following

from django_run_queues import tasks

# add a task

taskid = task.add('taskname','queuename', {parms in a Dictionary})

# get Results

# this will fail if the task has not run or is running. It will not wait

results = task.get_results(taskid)


RunQueue command
----------------

You can proccess tasks marked for a given queue name using the following command

python manage.py RunQueue --queue 'queuename'

With Cron, you can set this command to run repeatedly, or at different times for different queues

Thus to run a command to send out emails every 10 minutes

*/10 * * * * . env/bin/activate python manage.py RunQueue --queue 'EmailTaskQueue'


